.\"This file is part of Xcolorsel, a multipurpose viewer for X11 rgb.txt files.
.\"                    Copyright (C) 1993, 1994 Michael Weller
.\"
.\"This program is free software; you can redistribute it and/or modify it under
.\"the terms of the GNU General Public License as published be the Free Software
.\"Foundation;  either version 2 of the License,  or (at your opinion) any later
.\"version.
.\"
.\"This program is  distributed in the hope  that it will be useful, but WITHOUT
.\"ANY WARRANTY; without even the  implied warranty of  MERCHANBILITY or FITNESS
.\"FOR  A  PARTICULAR  PURPOSE.   See the  GNU General  Public License  for more
.\"details.
.\"
.\"You should have received a copy  of the GNU General Public License along with
.\"this program; if not,  write to the  Free Software Foundation, Inc., 675 Mass
.\"Ave, Cambridge, MA 02139, USA.
.\"
.\"The author can be  reached by means of email  as eowmob@exp-math.uni-essen.de
.\"or  eowmob@pollux.exp-math.uni-essen.de   or   mat42b@vm.hrz.uni-essen.de  or
.\"mat42b@de0hrz1a.bitnet.  Or as:  Michael Weller,  Heiderhoefen 116b,  D 46049
.\"Oberhausen, Germany.
.\"(See  other source files or  01-COPYING or  later in this file for additional
.\" info!)
.TH Xcolorsel 1 "27th December 1993" "" "Xcolorsel User's Guide"
.SH NAME
xcolorsel \- display contents of rgb.txt files together with color probes.
.SH SYNOPSIS
.B xcolorsel [options]
.BI [ RgbTxtFile ]
.SH DESCRIPTION
This utility displays the contents of the
.I rgb.txt
file (usually found as
.IR /usr/lib/X11/rgb.txt )
or a file with this format specified as the optional
.BI [ RgbTxtFile ]
parameter. Each line of this file consists of 3 integers
(usually 0-255) representing the RGB values and a nickname for this
color separated by spaces. The RGB values can be displayed in every
color representation X11R5/6 supports. Also it is possible to show
the values in either resource definition or
.BR C -style.
An additional option to display the RGB values the current server would
be able to actually display is also implemented. The main feature of
Xcolorsel is that a small example tile of each color is also shown.
I will describe each button/X11-control in Xcolorsel's window in the
next section.
.LP
This version has now also support for servers with read only colormaps (That is
StaticColor, StaticGray, TrueColor). Actually I did not realize that this would
be needed before I got a TrueColor server myself. However since colors have to
be allocated one by one in these modes, the startup time of xcolorsel has
reduced dramatically.
.SH USAGE
Xcolorsel's window is divided into four main parts:
.HP
(Usually) two rows of command buttons.
.HP
The RgbText Widget displaying the input file and (if needed) vertical and/or
horizontal scrollbars.
.HP
A message line displaying help messages, comments, or results of commands or
actions.
.HP
(Usually) one row of action buttons.
The main (or better intended) difference
between commands and actions is that action operate on colors selected in
the display area and commands perform general tasks.
.LP
The title bar of Xcolorsel's window should (if supported by the window manager)
show the name of the current displayed file.
.LP
Depending from the window size the actual layout may differ. Also the names
of the buttons may differ (if for example a german application default file is used).
However the position and order of the buttons and other elements should be the same,
s.t. you may rely on the order in which the controls are described in this file.
.SS Command Buttons (first row)
.B About me
opens a new window displaying an ASCII-Version of this manual page. When this window
is open the
.B About me
is inactive. In the lower left edge of this window a
.B Dismiss
button can be seen. Press this button to dismiss the help window. If the window is
to small to display the whole file vertical and/or horizontal scrollbars are provided.
The text is displayed in a standard Xaw AsciiText Widget. Text selection and <Ctrl>-S
(search) work as usual. For a full description of the capabilities of this Widget
see the Athena Widget Reference Manual. If the help window is dismissed the
.B About me
button is reactivated.
.LP
.B Fileinfo
displays the number of entries in the input file. Also the number of different
RGB triples is displayed. Furthermore the Xserver is queried to see how many
different colors the current server will show if given all these values.
.LP
The last value will differ depending on the color depth of the colormaps on the server.
Xfree86 Servers usually use 6 bit for each RGB value yielding to a lower number of
different colors on the server than different colors are given in the input file where
RGB values are usually 8 bit each.
.LP
.BR "Grab color" .
When you press this button, the cursor changes into a magnification glass with a
transparent hole at its hot spot. Move this hole over any colored pixel in any window
you are interested in and press the left button. While the cursor is a magnifying glass
Xcolorsel takes over the whole mouse. Xcolorsel itself ensures that each window the cursor
is in is displayed with its correct colormap.
.LP
After you pressed the left button the color selected is searched in the input file.
The color definition in the current
.B display format
is given in the message line. Also the number of acceptable colors, exact matches and
percentage of equality of the best match are given (if any). (Exception: If
.B display format
is
.I "values from input file"
each of the 16 bit RGB values is given like with
.I "16 bit scaled rgb"
but in decimal.)
.LP
Also the line containing the best match found is selected in the RgbText Widget, and
the text is scrolled, s.t. the line can be seen. The
.BR "Best match" ", " "Previous" ", and " Next
actions are appropriately activated.
.LP
The algorithm to compare colors is (too!) simple. Two colors
.B R1 G1 B1
and
.B R2 G2 B2
are compared by calculating
.BR "(R1-R2)*(R1-R2) + (G1-G2)*(G1-G2) + (B1-B2)*(B1-B2)" .
The result is scaled to 0-100%, where 100% means
equivalence and 0% is the difference between black and white
(aka between (0,0,0) and (255,255,255)). It is ensured that even almost
equal colors compare only up to 99.99%. 100% is only possible for
exact equivalence. The value above which colors are acceptable equal
can be changed and defaults to 95%.
.LP
Grabbed colors are compared against the color values as they can be
realized on the current server not the theoretic values from the input file.
.LP
.B Reverse
exchanges the text fore- and background color in the RgbText Widget.
.LP
.B Default colors
returns the text fore- and background color in the RgbText Widget to the values
at the start of Xcolorsel. This is (together with
.BR Reverse )
useful to return to readable text after you changed to colors with the
.BR "Set foreground" " and " "Set background"
actions to values making it impossible to read anything in the RgbText Widget.
.LP
.B Quit
quits Xcolorsel and closes all its windows.
.SS Command Buttons (second row)
.B Display format
displays a submenu that allows you to define the format in which color
RGB values are displayed (Press and hold left button down over
.B Display format
button and release the button over the desired format.
.LP
The possible color values are the color formats that X11R5/6 supports.
.I Values from input file
displays the RGB values like given in the input file (see also
comment in
.BR "Grab color" .
.LP
Note that conversion to
.I TekHVC
is slow for not trivially short input files.
.LP
While the color conversion calculation takes place the cursor in the
xcolorsel windows (except scrollbars) is changed to a busy cursor and
all controls are inactive.
.LP
Note the difference between scaled and truncated rgb formats: Scaled
scales the internal 16 bit RGB integer values to 4/8/12 bits whereas
truncated means the most significant 4/8/12 bits.
Truncated RGB values are only supported for compatibility. Their
use in new programs is discouraged.
.LP
.B C-Style
is a toggle control switching
.B C
support on/off. With
.B C
support RGB values are separated with , instead of the /
used in X-Windows resource definitions. Also Hex-Numbers are
given in the format that is needed in
.B C
source codes. Together with the selection capabilities of the
RgbText widget this allows inclusion of color definitions in programs
or Application Default Files. See next sub section.
.LP
This option is meaningless with the
.I Values from input file
.BR "display format" .
.LP
.B Show real server values
is again a toggle control. If enabled the server is queried which RGB values
for each color it is actually able to display. Then these values are displayed
instead of the theoretic values calculated by Xcolorsel and Xlib itself.
.LP
Note the when searching/grabbing colors Xcolorsel always compares with the
color values like they would be used by the server, because the searched color
is also only known by its actually realized values.
.LP
This option is meaningless with the
.I Values from input file
.BR "display format" .
.LP
.SS The RgbText Widget Display area
In this box (part of) the input file is displayed. Depending of the length and
width of the displayed information scrollbars are provided. Each line consists
of an example tile of the color, the defining color values in the selected
output format, and the nickname of the color.
.LP
The RgbText Widget inherits all capabilities of the AsciiText Widget, s.t.
scrollbars, <Ctrl>-S (searching) and selection work as usual. There is only
one exception:
.LP
Double clicking with the left button does not select words separated by
white space but here senseful subparts. That is double clicking in the
nickname selects the whole nickname regardless of included whitespace.
Double clicking on the color definitions selects the whole color definition.
If
.B C-Style
is selected the color format prefix is not selected, because it would not be
useful to paste this into a
.BR C -source
file.
.LP
This is done to make cutting and pasting color definitions into source codes
or Application Default Files as convenient as possible.
.SS The message line
This line contains one line messages informing you about the state and results
of Xcolorsel's actions.
.SS Action buttons
.B Best match
selects and scrolls the display to the best match found during the last
.BR "Grab color" ,
if one was found and is not currently selected.
.LP
.B Previous
selects and scrolls the display to the previous (next better) match,
if there is one.
.LP
.B Next
selects and scrolls the display to the next (next worse) match,
if there is one.
.LP
.B Set foreground
sets the text foreground color in the display window to the color currently selected
in it (actually the color defined in the line where the first selected character
resides).
.LP
.B Set background
sets the text background color in the display window to the color currently selected
in it (actually the color defined in the line where the first selected character
resides).
.SH OPTIONS
As a standard Xaw application Xcolorsel accepts all standard toolkit options
.RB "(see " X (1)).
.LP
Additional options for Xcolorsel that can also be set via Xresources (mentioned in
parentheses) in the
application default file (or with general resource manager facilities) are:
.HP
.BI "-breakpercentage" " n"
.RI ( "*.breakPercentage: n" )
sets the break percentage to
.IR n "/100 (" n " integer)."
Colors with equivalence equal to or above this value are found and
displayed when grabbing colors. (see
.B Grab color
above). This option defaults to 9500.
.HP
.BI "-busycursor" " cursor"
.RI ( "*.busyCursor: cursor" )
allows you to specify the cursor shown while calculating color conversions.
.HP
.B -color
.RI ( "*.customization: -color" ) 
sets the customization resource to force use of the Xcolorsel-color
application default file. If no application resource file can be found
at all, the compiled in Xcolorsel-color defaults are used.
.HP
.BI "-columns" " n"
.RI ( "*.columns: n" )
sets the number of character columns initially displayed. This option usually
defaults to 30.
.HP
.BI "-cstyle"
.RI ( "*.comma: True" )
lets Xcolorsel startup with
.B C-Style
selected.
.HP
.BI "-file" " filename"
.RI ( "*.file: filename" )
sets the file to be read in and displayed. Default is usually
.RI "" /usr/lib/X11/rgb.txt .
You can specify the filename as command parameter without the
prefixing
.BR -file .
.HP
.BI "-grabcursor" " cursor"
.RI ( "*.grabCursor: cursor" )
allows you to specify the cursor shown while grabbing colors.
.HP
.BI "-helpfile" " filename"
.RI ( "*.helpfile: filename )
sets the path to the helpfile. (can also be done at compile time in config.h).
If set to the empty string ("", the default). Xcolorsel tries to locate
the file with standard X techniques. This usually leads to filenames like
.IR /usr/lib/X11/Xcolorsel.help " or " /usr/lib/X11/app-defaults/Xcolorsel.help .
Just trust the make install created by imake. It should know where to put
the file. However people (read: vendor specific X11 releases) had problems with this
so
.I /usr/lib/X11/xcolorsel/Xcolorsel.help
is now hardcoded in the Imakefile.
.HP
.BR -input ", " -rgb4 ", " -rgb8 ", "
.BR -rgb12 ", " -rgb16 ", " -rgbi ", "
.BR -ciexyz ", " -cieuvy ", " -ciexyy ", "
.BR -cielab ", " -cieluv ", " -tekhvc ", "
.BR -#4 ", " -#8 ", " -#12 ", and "
.B -#16
.RI ( "*.displayFormat: format" ", where " format
is one of
.RI "" input ", " rgb4 ", " rgb8
.RI ", " rgb12 ", " rgb16 ", " rgbi
.RI ", " ciexyz ", " cieuvy ", " ciexyy
.RI ", " cielab ", " cieluv ", " tekhvc
.RI ", " #rgb ", " #rrggbb ", " #rrrgggbbb
.RI ", or " #rrrrggggbbbb )
lets Xcolorsel startup displaying in the specified color format.
The default and value chosen if set to an undefined string value is
.RI "" input .
.HP
.BI "-internal"
.RI ( "*.realized: False" )
lets Xcolorsel startup showing the internal theoretic
color values. This is the default.
.HP
.BI "-maxred" " n"
.RI ( "*.maxRed: n" )
.HP
.BI "-maxgreen" " n"
.RI ( "*.maxGreen: n" )
.HP
.BI "-maxblue" " n"
.RI ( "*.maxBlue: n" )
When reading in the input file Xcolorsel calculates the maximum of each of the
input RGB values and uses this as the maximum (white) value for this component.
Each of these option values is incorporated in this calculation for the
appropriate color component as if such a color would have been read from the
input file.
This is implemented to allow input file with more (or less) than 8 bit for each
RGB value. This options default to 0 but note the next option:
.HP
.BI "-maxintensity" " n"
.RI ( "*.maxIntensity: n" )
is comparable to the last three options but is incorporated into each
of the color components. This value usually defaults to 255.
.HP
.BI "-maxrows" " n"
.RI ( "*.maxRows: n" )
sets the maximum number of rows to be displayed at the same time. This restriction
is needed because this many color cells have to be allocated at the Xserver and
the number of colorcells is usually limited (usually to 256). If Xcolorsel can't
allocate this much colorcells a lower number is chosen and the user is informed
via stderr. Only if not a single color can be allocated Xcolorsel gives up.
Xcolorsel instructs the window manager not to allow resizes yielding to more lines
getting displayed. If more lines are displayed in the display RgbText Widget area the
displayed color examples will be wrong. This option usually defaults to
30.
.HP
.B -nocolor
.RI ( "*.customization: ''" )
sets the customization resource to force use of the non-color Xcolorsel
application default file. If no application resource file can be found
at all, the compiled in non-color Xcolorsel defaults are used.
BTW, actually you have to use " to specify this in resource files.
.HP
.BI "-realized"
.RI ( "*.realized: True" )
lets Xcolorsel startup showing color values how they are
displayed on the server.
.HP
.BI "-resourcestyle"
.RI ( "*.comma: False" )
lets Xcolorsel startup with
.B C-Style
deselected. This is the default.
.HP
.BI "-rows" " n"
.RI ( "*.rows: n" )
sets the number of character rows initially displayed. This option usually
defaults to 10.
.HP
.BI "-tilewidth" " n"
.RI ( "*.tilewidth: n" )
sets the width of the example color tiles measured in spaces. This option usually
defaults to 3.
.SH HISTORY
Xcolorsel was named xpalette when I wrote it. However there were so many
xpalettes that I was asked to change its name. Since xpalette is somehow
comparable to xfontsel, that is it shows you the available colors and lets
you select one, I call it xcolorsel now and hope that there will be no more
confusion. So when you see a
.IR xpalette.tgz ,
or
.IR xpalette-1.0.tar.gz ,
or
.IR xpalette-1.0a.tar.gz
it is probably a previous version of xcolorsel.
.SH INTERNALS
Most of the functionality of Xcolorsel is defined in an RgbText Widget.
This is a child of a TextWidget linking an RgbSrc and RgbSink Widget together, which
are childs of AsciiSrc and AsciiSink, resp. Feel free to use these
Widgets in your own programs. Most of Xcolorsel's resources are resources of these
Widgets. Interested programmers should be able to find all needed information
about the programming interface of these Widgets in the corresponding
.I .h
.RI "files (" RgbSink.h ", " RgbSinkP.h
.RI ", " RgbSrc.h ", " RgbSrcP.h
.RI ", " RgbText.h ", " RgbTextP.h ")."
.LP
Anyway, I would love to hear if anyone finds a general purpose of any of those
widgets. (I can't think of any ;-) ).
.LP
If you want to know about the internal structure and names of the Widget tree
use
.BR editres (1),
which is part of X11R5/6 and shows this information in a more convenient way
than I could ever put in the manual page.
.SH RESTRICTIONS/MISFEATURES
.HP
Installing the colormaps of windows the cursor is in during grab of colors is slow.
This is because the Athena Widgets intercept
.B ENTER/LEAVE
notifications send to Xcolorsel during a pointer grab. As a work around Xcolorsel
asks for notification of all cursor motions during a pointer grab and explicitly
queries the X-Window the pointer is in, thus causing ex(t/p)ensive server/client
communication.
.HP
Color conversion calculations are even performed when there is no need to, that is
when just the
.B C-Style
is toggled. This is really annoying when using the
.I TekHVC
.BR "display format" .
.HP
When
.BR "Display format" ", " "C-Style" ", or " "Show real server values"
are changed the current selection is unselected and the display scrolls up to the
first line.
.HP
.RB "The " "Best match" ", " "Previous" ", and " "Next"
action buttons are not appropriately (de-)activated when the selection is either
changed manually in the display with the mouse or automatically (see above
misfeature).
.HP
The icons and cursors (especially the bigger magnifying glass) are extremely ugly.
Suggestions for nice bitmaps are welcome.
.HP
The color application default is even more ugly. It is an example of what not to do
with Xcolorsel. Actually the intention was to classify operations with colors.
(Negative commands like
.B Quit
in red, positive commands like
.B Default colors
in bright or not that bright
.RB ( "About me" )
green. Commands requiring user intervention bright yellow. Other commands in standard
yellow. And the remaining toggles and actions in orange.)
However the result just makes me puke. Suggestions for nice colors are also welcome.
.HP
If the help window is displayed
.B About me
should not be inactive but instead allow to raise the window.
.HP
The color compare formula (see
.B Grab color
above) is (at least) not very sophisticated.
.HP
When showing the busy cursor during color conversions the standard cursor is still
shown when over scrollbars. This is because I didn't find a way to change the cursor
over Athena scrollbars. Later I saw that one can of course hide all windows under
an invisible input only window that changes the cursor when it enters it. (I got
this from Mumail, where unfortunately only part of the window is hidden under the
invisible window).
.HP
You will notice that my programming style changed while coding Xcolorsel, thus
the look a like of the sources is not as consistent as it should be.
.SH BUGS
Under AIX (IBM RS/6000) at least when using 16 color monochrom monitors and the
Motif window manager, searching the window tree sometimes finds windows with
invalid colormaps and that are not drawable. There is a flag to not to try
to install colormaps during color grabbing. However if you click on a window
border Xcolorsel will crash. I assume that this is caused by mwm placing an
invisible input only window over the borders it draws. I did not find a way
to make Xcolorsel smart enough to not fall in this trap.
Even if Xcolorsel would see that this is an input only window, there is no
way to get the data it actually needs (colormap and pixelvalue at the
location you are clicking on).
Just do not click on Mwm borders. Or better do not use Mwm at all. Motif
is no free software. Boycott it! Fvwm is much better anyway.
.SH AUTHOR
Michael Weller
.br
Heiderhoefen 116b
.br
D 46049 Oberhausen
.br
Germany
.LP
reachable by means of Email as one of:
.LP
eowmob@exp-math.uni-essen.de
.br
eowmob@pollux.exp-math.uni-essen.de
.br
mat42b@aixrs1.hrz.uni-essen.de
.LP
Please note that I did this as an exercise to improve my understanding of the
Athena Widgets and X. I consider this project as (almost) finished. The priority
of this project is as low as possible for me. No further extensions are planned
so far. However I will put in patches to support other systems (please make
them controllable with #define's either out of config.h or provided by your
imake) and bug fixes and redistribute them to ensure that only one consistent
version floats through the net.
.
.SH ACKNOWLEDGEMENTS
This work and much more would not have been possible without the Linux
project initiated by Linus Torvalds (and of course all other people
contributing their work to this project). This project together with the
GNU project made it possible for capable computer users of my generation
to choose a free, reliable and state of the art operating system and to
get rid of the arbitrariness of commercial software vendors and business
people squeezing just money out of people that have to rely on them
instead of supplying working and useful software.
.LP
I have much experience with different home computer OS's and workstation
UNIX implementations. You can trust me. Just use linux and get rid of
all your problems (What a pity it runs only on PCs and not on higher
performance workstations)
.LP
Thanx go also to Nils Rennebarth who convinced me I should implement
the colormap install feature during color grabbing and made me develop
a way to accomplish this in a fruitful discussion.
.LP
Even more thanx to R.K.Lloyd@csc.liv.ac.uk (no RL name known) for his
patches that are needed to allow compilation under HP-UX (but changed
two of his patches that disabled grabbing of colors by accident ;-) and
Chris Olson (chris@Sterling.COM) for pointing me at the compilation
problems on SUN's (but who would use a Sun? >;-> ).
.LP
Thanks and greetings go to Chris Rouch (chris@wg.estec.esa.nl). He was
the first person that could provide a patch to make xcolorsel run on a
Sun. Actually the patch had a bug as well, however it did show where to
look at, so the problem could be solved in seconds. Many other people
(far too many to list here) sent me detailed debug sessions from their
machines that approved the detected bug. Thanks to all these as well.
.LP
Maybe I should have announced xcolorsel louder when it still was called
xpalette. This way the long existing segmentation fault problem would
have been found earlier. Many people searching for a bug on several
machines are obviously more succesful than a single one.
.LP
Thus once again the power of the Internet community was shown.
.SH COPYRIGHTS
As I strongly believe in the usefulness of free software this program
is released under the GNU general public license:
.LP
This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published be the Free Software
Foundation;  either version 2 of the License,  or (at your opinion) any later
version.
.LP
This program is  distributed in the hope  that it will be useful, but WITHOUT
ANY WARRANTY; without even the  implied warranty of  MERCHANBILITY or FITNESS
FOR  A  PARTICULAR  PURPOSE.   See the  GNU General  Public License  for more
details.
.LP
You should have received a copy  of the GNU General Public License along with
this program; if not,  write to the  Free Software Foundation, Inc., 675 Mass
Ave, Cambridge, MA 02139, USA.
.LP
Part of the files are derived  from the Template Widget of the Athena Widgets
as published  by the  Massachusetts Institute of  Technology.  Actually these
files do not contain any code  by the M.I.T  but only  variable declarations.
Nevertheless these parts of these files are still distributed under the terms
of the M.I.T copyright which are here repeated:
.LP
Copyright    Massachusetts Institute of Technology   1987, 1988
.LP
Permission to use, copy,  modify, distribute,  and sell this software and its
documentation for  any purpose is hereby  granted without fee,  provided that
the  above  copyright  notice  appear  in  all  copies  and  that  both  that
copyright   notice   and   this  permission   notice  appear  in   supporting
documentation,  and that the  name of M.I.T.  not  be used  in advertising or
publicity  pertaining  to distribution  of  the  software  without  specific,
written  prior  permission.    M.I.T.  makes  no  representations  about  the
suitability  of  this  software  for  any  purpose.   It is provided  "as is"
without express or implied warranty.
.LP
M.I.T. DISCLAIMS ALL  WARRANTIES WITH REGARD  TO THIS SOFTWARE, INCLUDING ALL
IMPLIED WARRANTIES  OF MERCHANTABILITY AND FITNESS,  IN NO EVENT SHALL M.I.T.
BE LIABLE FOR ANY SPECIAL,  INDIRECT OR CONSEQUENTIAL  DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE,  DATA OR PROFITS, WHETHER IN AN ACTION
OF CONTRACT,  NEGLIGENCE  OR  OTHER  TORTIOUS  ACTION,  ARISING OUT OF  OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
.SH FILES
.I /usr/lib/X11/rgb.txt
.br
.I /usr/lib/X11/xcolorsel/Xcolorsel.help
.br
.I /usr/lib/X11/app-defaults/Xcolorsel 
.br
.I /usr/lib/X11/app-defaults/Xcolorsel-color
.SH SEE ALSO
.BR X "(1), " xcolormap "(1), " xfontsel (1),
.BR editres (1)
